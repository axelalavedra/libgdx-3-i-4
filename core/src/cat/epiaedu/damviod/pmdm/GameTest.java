package cat.epiaedu.damviod.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class GameTest implements ApplicationListener , InputProcessor{
	SpriteBatch batch;
	Texture img;
	TextureAtlas textureAtlas;
	Animation animation;
	Rectangle sprite;
	TextureRegion texture;
	float movementSpeed;
	boolean flip;
	Sound engineSound;
    Sound runningSound;
    float enginespeed;
    float maxSpeed;
    float minSpeed;
    float acceleration;
    boolean idle;

    float pitch;
    long soundID;
	float elapsedTime;
	float lastTime;
    boolean running;
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		Gdx.app.log("TEst","create");
		textureAtlas = new TextureAtlas(Gdx.files.internal("atlas/pack.atlas"));
		animation = new Animation(1/15f, textureAtlas.getRegions());
		sprite = new Rectangle();
		movementSpeed = 0f;
		Gdx.input.setInputProcessor(this);
		flip = false;
		sprite.x = 200;
		sprite.y = 200;
		texture = new TextureRegion();
		engineSound = Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
        runningSound = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
        soundID = engineSound.play();
        engineSound.setLooping(soundID, true);
        pitch = 0;
        enginespeed = 0;
        maxSpeed = 200;
        minSpeed = 0;
        acceleration = 150f;
        idle=true;
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("TEst","resize");
	}

	@Override
	public void render () {

		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		sprite.x += movementSpeed * lastTime;

        if(running){
            if(enginespeed<maxSpeed) enginespeed += acceleration * lastTime;
            if(enginespeed>maxSpeed) enginespeed = maxSpeed;
        }
        else {
            if(!idle){
                enginespeed -= acceleration * lastTime;
                if (enginespeed <= minSpeed) {
                    runningSound.stop(soundID);
                    soundID = engineSound.play();
                    engineSound.setLooping(soundID, true);
                    enginespeed = minSpeed;
                    idle = true;
                }
            }
        }
        pitch = 0.5f + enginespeed / maxSpeed * 0.5f;
        runningSound.setPitch(soundID, pitch);

		batch.begin();
		texture.setRegion((TextureRegion)animation.getKeyFrame(elapsedTime,true));
		texture.flip(flip,false);
		elapsedTime += Gdx.graphics.getDeltaTime();
		lastTime = Gdx.graphics.getDeltaTime();
		batch.draw(texture,sprite.x,sprite.y);
		batch.end();
	}

	@Override
	public void pause() {
		Gdx.app.log("TEst","pause");
	}

	@Override
	public void resume() {
		Gdx.app.log("TEst","resume");
	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {

		if(keycode == Input.Keys.LEFT)
		{
			flip = true;
			movementSpeed = -500f;
		}
		if(keycode == Input.Keys.RIGHT)
		{
			flip = false;
			movementSpeed = 500f;
		}
		if(keycode == Input.Keys.SPACE){
			engineSound.stop(soundID);
            running = true;
            soundID = runningSound.play();
            runningSound.setLooping(soundID,true);
            idle = false;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		movementSpeed = 0f;
        if(keycode == Input.Keys.SPACE) running = false;

		return false;

	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button ==0){
			sprite.x = screenX - texture.getRegionWidth()/2;
			sprite.y = (Gdx.graphics.getHeight() - screenY) - texture.getRegionHeight()/2;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
